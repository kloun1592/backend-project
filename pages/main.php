<?php
    require_once '../libs/include/consts/db_consts.php';
    require_once '../libs/include/database.inc.php';
    session_start();
    if (!isset($_SESSION['project_id'])) 
    {
        header('Location: http://localhost:8888/onShopGen/');
    }
    else
    {
        dbInitialConnect(DATABASE);
        $query = "SELECT shop_created, user_name FROM users WHERE project_id = '{$_SESSION['project_id']}'";
        $array = dbQueryGetResult($query);
        dbConnectClose();
        if ($array[0]['shop_created'] == 1 || count($array) == 0) 
        {
            header('Location: http://localhost:8888/onShopGen/');
        }
    }
?>

<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Генератор интернет-магазина</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="../web/css/material.indigo-pink.min.css">
    <link rel="stylesheet" href="../web/css/main.css">
    <link rel="stylesheet" href="../web/css/photoswipe.css">
    <link rel="stylesheet" href="../web/css/default-skin/default-skin.css">
    <link rel="stylesheet" href="../web/css/animate.css">
  </head>
  <body>
    <div class="mdl-layout mdl-js-layout">
      <main class="mdl-layout__content">
        <form action="main2.php" method="GET">
          <div class="mdl-grid shop_type">
            <div class="mdl-card-wide mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col animated fadeInUp">
              <div class="mdl-card__title">
                <h2 class="mdl-card__title-text">Здравствуйте, <?= $array[0]['user_name'] ?>!</h2>
              </div>
              <div class="mdl-card__title">
                <h2 class="mdl-card__title-text">Вы решили создать интернет-магазин. Это правильное решение! <br/> Для начала выберите необходимый для Вас тип интернет-магазина.</h2>
              </div>
            </div>
            <div class="card-square mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col animated fadeInUp">
              <div class="mdl-card__title mdl-card--expand first">
                <a href="#" class="shopImageFirst"></a>
              </div>
              <div class="mdl-card__actions mdl-card--border">
                <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-1">
                  <input type="radio" id="option-1" class="mdl-radio__button" name="shopType" value="1" checked>
                  <span class="mdl-radio__label">Первый</span>
                </label>
              </div>
            </div>
            <div class="card-square mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col animated fadeInUp">
              <div class="mdl-card__title mdl-card--expand second">
                <a href="#" class="shopImageSecond"></a>
              </div>
              <div class="mdl-card__actions mdl-card--border">
                <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-2">
                  <input type="radio" id="option-2" class="mdl-radio__button" name="shopType" value="2" disabled readonly>
                  <span class="mdl-radio__label">Второй</span>
                </label>
              </div>
            </div>
            <div class="card-square mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col animated fadeInUp">
              <div class="mdl-card__title mdl-card--expand third">
                <a href="#" class="shopImageThird"></a>
              </div>
              <div class="mdl-card__actions mdl-card--border">
                <label class="mdl-radio mdl-js-radio mdl-js-ripple-effect" for="option-3">
                  <input type="radio" id="option-3" class="mdl-radio__button" name="shopType" value="3" disabled readonly>
                  <span class="mdl-radio__label">Третий</span>
                </label>
              </div>
            </div>
          </div>
          <div class="clear"></div>
          <button class="nextButton mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored">
          Дальше
          </button>  
        </form>
      </main>
    </div>
    <a href="http://localhost:8888/onShopGen/libs/logout.php" class="logout mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored exitButton">Выйти
      <span class="mdl-button__ripple-container">
        <span class="mdl-ripple is-animating"></span>
      </span>
    </a>
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="pswp__bg"></div>
      <div class="pswp__scroll-wrap">
        <div class="pswp__container">
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
          <div class="pswp__item"></div>
        </div>
        <div class="pswp__ui pswp__ui--hidden">
          <div class="pswp__top-bar">
            <div class="pswp__counter"></div>
            <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
            <button class="pswp__button pswp__button--share" title="Share"></button>
            <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
            <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
            <div class="pswp__preloader">
              <div class="pswp__preloader__icn">
                <div class="pswp__preloader__cut">
                  <div class="pswp__preloader__donut"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
            <div class="pswp__share-tooltip"></div>
          </div>
          <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
          </button>
          <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
          </button>
          <div class="pswp__caption">
            <div class="pswp__caption__center"></div>
          </div>
        </div>
      </div>
    </div>
    <script defer src="../web/js/material.min.js"></script>
    <script src="../web/js/jquery.min.js"></script>
    <script src="../web/js/photoswipe.min.js"></script>
    <script src="../web/js/photoswipe-ui-default.min.js"></script>
    <script src="../web/js/openSwipe.js"></script>
    <script src="../web/js/mdl-jquery-modal-dialog.js"></script>
    <script src="../web/js/goToShopElements.js"></script>
  </body>
</html>