<?php
    require_once '../libs/include/consts/db_consts.php';
    require_once '../libs/include/database.inc.php';
    session_start();
    if (!isset($_SESSION['project_id'])) 
    {
        header('Location: http://localhost:8888/onShopGen/');
    }
    else
    {
        dbInitialConnect(DATABASE);
        $query = "SELECT shop_created, user_name FROM users WHERE project_id = '{$_SESSION['project_id']}'";
        $array = dbQueryGetResult($query);
        dbConnectClose();
        if ($array[0]['shop_created'] == 1 || count($array) == 0) 
        {
            header('Location: http://localhost:8888/onShopGen/');
        }
    }
?>
<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Генератор интернет-магазина</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="../web/css/material.indigo-pink.min.css">
    <link rel="stylesheet" href="../web/css/main.css">
    <link rel="stylesheet" href="../web/css/mdl-jquery-modal-dialog.css">
    <link rel="stylesheet" href="../web/css/animate.css">
  </head>
  <body>
    <div class="mdl-layout mdl-js-layout">
      <main class="mdl-layout__content">
        <form action="../libs/shopGenerate.php" method="POST">
          <div class="mdl-grid">
            <div class="mdl-card-wide mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col animated fadeInUp custom">
              <div class="mdl-card__title custom">
                <h2 class="mdl-card__title-text">Отлично! Теперь выберите элементы интернет-магазина, которые вы хотите в него включить.<br/>P.S. Необходимый минимум уже включён в список.</h2>
              </div>
            </div>
            <div class="mdl-card-wide mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col lists animated fadeInLeft" >
              <div class="mdl-card__title">
                <h2 class="mdl-card__title-text">Главная страница</h2>
              </div>
              <div class="mdl-card__supporting-text">
                <ul class="list-control mdl-list">
                  <li class="mdl-list__item">
                    <span class="mdl-list__item-primary-content">
                    Основное меню
                    </span>
                    <span class="mdl-list__item-secondary-action">
                      <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="list-checkbox-2">
                        <input type="checkbox" name="optionList[]" value="mainMenu" id="list-checkbox-2" class="mdl-checkbox__input"/>
                      </label>
                    </span>
                  </li>
                  <li class="mdl-list__item">
                    <span class="mdl-list__item-primary-content">
                    Меню сайта и контактная информация внизу сайта ("подвал" или footer)
                    </span>
                    <span class="mdl-list__item-secondary-action">
                      <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="list-checkbox-9">
                        <input type="checkbox" name="optionList[]" value="footer" id="list-checkbox-9" class="mdl-checkbox__input" />
                      </label>
                    </span>
                  </li>
                  <li class="mdl-list__item">
                    <span class="mdl-list__item-primary-content">
                    Слайдер изображений на главной странице
                    </span>
                    <span class="mdl-list__item-secondary-action">
                      <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="list-checkbox-3">
                        <input type="checkbox" name="optionList[]" value="sliderMainPage" id="list-checkbox-3" class="mdl-checkbox__input" />
                      </label>
                    </span>
                  </li>
                  <li class="mdl-list__item">
                    <span class="mdl-list__item-primary-content">
                    Специальные предложения для покупателей на главной странице
                    </span>
                    <span class="mdl-list__item-secondary-action">
                      <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="list-checkbox-4">
                        <input type="checkbox" name="optionList[]" value="specialOffers" id="list-checkbox-4" class="mdl-checkbox__input" />
                      </label>
                    </span>
                  </li>
                  <li class="mdl-list__item">
                    <span class="mdl-list__item-primary-content">
                    Большие баннеры для перехода в специальные категории
                    </span>
                    <span class="mdl-list__item-secondary-action">
                      <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="list-checkbox-6">
                        <input type="checkbox" name="optionList[]" value="bigCategoriesBanners" id="list-checkbox-6" class="mdl-checkbox__input" />
                      </label>
                    </span>
                  </li>
                  <li class="mdl-list__item">
                    <span class="mdl-list__item-primary-content">
                    Видео на главной странице
                    </span>
                    <span class="mdl-list__item-secondary-action">
                      <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="list-checkbox-7">
                        <input type="checkbox" name="optionList[]" value="videoBanner" id="list-checkbox-7" class="mdl-checkbox__input" />
                      </label>
                    </span>
                  </li>
                  <li class="mdl-list__item">
                    <span class="mdl-list__item-primary-content">
                    Товары со скидкой на главной странице
                    </span>
                    <span class="mdl-list__item-secondary-action">
                      <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="list-checkbox-8">
                        <input type="checkbox" name="optionList[]" value="salesMainPage" id="list-checkbox-8" class="mdl-checkbox__input" />
                      </label>
                    </span>
                  </li>
                </ul>
              </div>
            </div>
            <div class="mdl-card-wide mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col lists animated fadeInRight">
              <div class="mdl-card__title">
                <h2 class="mdl-card__title-text">Страница: Контакты</h2>
              </div>
              <div class="mdl-card__supporting-text">
                <ul class="list-control mdl-list">
                  <li class="mdl-list__item">
                    <span class="mdl-list__item-primary-content">
                    Адресс компании
                    </span>
                    <span class="mdl-list__item-secondary-action">
                      <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="list-checkbox-16">
                        <input type="checkbox" name="optionList[]" value="streetAddress" id="list-checkbox-16" class="mdl-checkbox__input" />
                      </label>
                    </span>
                  </li>
                  <li class="mdl-list__item">
                    <span class="mdl-list__item-primary-content">
                    Google Maps карта с адрессами
                    </span>
                    <span class="mdl-list__item-secondary-action">
                      <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="list-checkbox-20">
                        <input type="checkbox" name="optionList[]" value="googleMap" id="list-checkbox-20" class="mdl-checkbox__input" />
                      </label>
                    </span>
                  </li>
                  <li class="mdl-list__item">
                    <span class="mdl-list__item-primary-content">
                    Номер телефона компании
                    </span>
                    <span class="mdl-list__item-secondary-action">
                      <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="list-checkbox-17">
                        <input type="checkbox" name="optionList[]" value="companyPhoneNumber" id="list-checkbox-17" class="mdl-checkbox__input" />
                      </label>
                    </span>
                  </li>
                  <li class="mdl-list__item">
                    <span class="mdl-list__item-primary-content">
                    Почта компании
                    </span>
                    <span class="mdl-list__item-secondary-action">
                      <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="list-checkbox-18">
                        <input type="checkbox" name="optionList[]" value="emailAdress" id="list-checkbox-18" class="mdl-checkbox__input" />
                      </label>
                    </span>
                  </li>
                  <li class="mdl-list__item">
                    <span class="mdl-list__item-primary-content">
                    Форма отправки отзывов
                    </span>
                    <span class="mdl-list__item-secondary-action">
                      <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="list-checkbox-19">
                        <input type="checkbox" name="optionList[]" value="recallForm" id="list-checkbox-19" class="mdl-checkbox__input" />
                      </label>
                    </span>
                  </li>
                </ul>
              </div>
            </div>
            <div class="mdl-card-wide mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col lists animated fadeInUp">
              <div class="mdl-card__title">
                <h2 class="mdl-card__title-text">Страница: Каталог товаров</h2>
              </div>
              <div class="mdl-card__supporting-text">
                <ul class="list-control mdl-list">
                  <li class="mdl-list__item">
                    <span class="mdl-list__item-primary-content">
                    Список популярных категорий
                    </span>
                    <span class="mdl-list__item-secondary-action">
                      <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="list-checkbox-21">
                        <input type="checkbox" name="optionList[]" value="popularCategories" id="list-checkbox-21" class="mdl-checkbox__input" />
                      </label>
                    </span>
                  </li>
                  <li class="mdl-list__item">
                    <span class="mdl-list__item-primary-content">
                    Слайдер изображений на странице: Каталог товаров
                    </span>
                    <span class="mdl-list__item-secondary-action">
                      <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="list-checkbox-22">
                        <input type="checkbox" name="optionList[]" value="sliderCurretCategoryPage" id="list-checkbox-22" class="mdl-checkbox__input" />
                      </label>
                    </span>
                  </li>
                </ul>
              </div>
            </div>
            <div class="mdl-card-wide mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col lists animated fadeInUp">
              <div class="mdl-card__title">
                <h2 class="mdl-card__title-text">Страница: Часто задаваемые вопросы</h2>
              </div>
              <div class="mdl-card__supporting-text no-additional-lists">
                <span>Здесь нет дополонительных пунктов</span>
              </div>
            </div>
            <div class="mdl-card-wide mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col lists">
              <div class="mdl-card__title">
                <h2 class="mdl-card__title-text">Страница: Авторизация пользователя</h2>
              </div>
              <div class="mdl-card__supporting-text no-additional-lists">
                <span>Здесь нет дополонительных пунктов</span>
              </div>
            </div>
            <div class="mdl-card-wide mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col lists">
              <div class="mdl-card__title">
                <h2 class="mdl-card__title-text">Страница: Регистрация пользователя</h2>
              </div>
              <div class="mdl-card__supporting-text no-additional-lists">
                <span>Здесь нет дополонительных пунктов</span>
              </div>
            </div>
            <div class="mdl-card-wide mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col lists">
              <div class="mdl-card__title">
                <h2 class="mdl-card__title-text">Страница: Информация о доставке</h2>
              </div>
              <div class="mdl-card__supporting-text no-additional-lists">
                <span>Здесь нет дополонительных пунктов</span>
              </div>
            </div>
            <div class="mdl-card-wide mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col lists">
              <div class="mdl-card__title">
                <h2 class="mdl-card__title-text">Страница: Правила и политика</h2>
              </div>
              <div class="mdl-card__supporting-text no-additional-lists">
                <span>Здесь нет дополонительных пунктов</span>
              </div>
            </div>
            <div class="mdl-card-wide mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col lists">
              <div class="mdl-card__title">
                <h2 class="mdl-card__title-text">Страница отдельного товара</h2>
              </div>
              <div class="mdl-card__supporting-text">
                <ul class="list-control mdl-list">
                  <li class="mdl-list__item">
                    <span class="mdl-list__item-primary-content">
                    Товары со скидкой на странице отдельного товара
                    </span>
                    <span class="mdl-list__item-secondary-action">
                      <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="list-checkbox-14">
                        <input type="checkbox" name="optionList[]" value="salesCurrentProductPage" id="list-checkbox-14" class="mdl-checkbox__input" />
                      </label>
                    </span>
                  </li>
                </ul>
              </div>
            </div>
            <div class="mdl-card-wide mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col lists">
              <div class="mdl-card__title">
                <h2 class="mdl-card__title-text">Страница: О нас</h2>
              </div>
              <div class="mdl-card__supporting-text">
                <ul class="list-control mdl-list">
                  <li class="mdl-list__item">
                    <span class="mdl-list__item-primary-content">
                    История компании
                    </span>
                    <span class="mdl-list__item-secondary-action">
                      <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="list-checkbox-12">
                        <input type="checkbox" name="optionList[]" value="companyHistory" id="list-checkbox-12" class="mdl-checkbox__input" />
                      </label>
                    </span>
                  </li>
                  <li class="mdl-list__item">
                    <span class="mdl-list__item-primary-content">
                    Плюсы и достоинства компании
                    </span>
                    <span class="mdl-list__item-secondary-action">
                      <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="list-checkbox-13">
                        <input type="checkbox" name="optionList[]" value="companySkillAndAdvantages" id="list-checkbox-13" class="mdl-checkbox__input" />
                      </label>
                    </span>
                  </li>
                </ul>
              </div>
            </div>
            <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored">
            Сгенерировать!
            </button>  
          </div>
        </form>
      </main>
    </div>
    <a href="http://localhost:8888/onShopGen/libs/logout.php" class="logout mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored exitButton">Выйти
      <span class="mdl-button__ripple-container">
        <span class="mdl-ripple is-animating"></span>
      </span>
    </a>
    <script defer src="../web/js/material.min.js"></script>
    <script src="../web/js/jquery.min.js"></script>
    <script src="../web/js/mdl-jquery-modal-dialog.js"></script>
    <script src="../web/js/shopGenerate.js"></script>
  </body>
</html>