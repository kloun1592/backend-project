<?php
    const ERROR_MESSAGE = 'Что-то пошло не неправльно:( Попробуйте позже.';

    const SUCCESS_REQUEST_STATUS = 'requestSuccess';
    const UNSUCCESS_REQUEST_STATUS = 'badData';
    const REPEAT_LOGIN_STATUS = 'repetedLogin';
    const REPEAT_MAIL_STATUS = 'repetedMail';
    const UNIQUE_DATA_STATUS = 'dataUnique';