<?php
    const ARRAY_OF_OPTIONS = array(
                    'mainMenu' => false,
                    'sliderMainPage' => false,
                    'specialOffers' => false, 
                    'bigCategoriesBanners' => false,
                    'videoBanner' => false,
                    'salesMainPage' => false,
                    'salesCurrentProductPage' => false,
                    'footer' => false,
                    'upButton' => false,
                    'companyHistory' => false,
                    'companySkillAndAdvantages' => false,
                    'streetAddress' => false,
                    'companyPhoneNumber' => false,
                    'emailAdress' => false,
                    'recallForm' => false,
                    'googleMap' => false,
                    'popularCategories' => false,
                    'sliderCurretCategoryPage' => false
                    );
    const ARRAY_OF_ADDITIONAL_FILES_AND_FOLDERS = array(
                                         'fonts',
                                         'images',
                                         'js',
                                         'video'
                                         );
    const ARRAY_OF_MAIN_FILES = array(
                          'css' => array(
                                'style.css',
                                'bootstrap.css',
                                'font-awesome.css',
                                'jstarbox.css'
                                )
                           );
    const MAIN_PAGES = array(
                         'index',
                         'single',
                         'about',
                         'wishlist',
                         'contact',
                         'care',
                         'faqs',
                         'login',
                         'register',
                         'shipping',
                         'terms'
                         );