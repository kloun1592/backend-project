<?php
    function checkRepeatedData($login, $mail)
    {
        $res = UNIQUE_DATA_STATUS;
        $query = "SELECT user_name FROM users WHERE user_name = '" . $login . "'";
        if (!empty(dbQueryGetResult($query))) 
        { 
            $res = REPEAT_LOGIN_STATUS;
        }
        else
        {
            $query = "SELECT user_mail FROM users WHERE user_mail = '" . $mail . "'";
            if (!empty(dbQueryGetResult($query))) 
            { 
                $res = REPEAT_MAIL_STATUS;
            }
        }
        return $res;
    }

    function register($login, $password, $mail)
    {
        dbInitialConnect(DATABASE);
        $login = trim(destroyInjection($login));
        $password = trim(destroyInjection($password));
        $mail = trim(destroyInjection($mail));
        if (strlen($login) >= MIN_LOGIN_LENGTH && strlen($password) >= MIN_PASSWORD_LENGTH && filter_var($mail, FILTER_VALIDATE_EMAIL)) 
        {
            $projectId = md5($mail);
            $status = checkRepeatedData($login, $mail);
            if ($status == UNIQUE_DATA_STATUS)
            {
                $query = "INSERT INTO users (user_name, user_pass, user_mail, project_id) VALUES ('" . $login . "', '" . $password . "', '" . $mail . "', '" . $projectId . "')";
                dbQuery($query);
                createSession('project_id', $projectId);
                return $projectId;
            }
        }
        else
        {
            $status = UNSUCCESS_REQUEST_STATUS;
        }
        dbConnectClose();
        return $status;
    }

    function login($login, $password)
    {
        $res = UNSUCCESS_REQUEST_STATUS;
        dbInitialConnect(DATABASE);
    	$login = trim(destroyInjection($login));
        $password = trim(destroyInjection($password));
    	$query = "SELECT project_id FROM users WHERE user_name = '" . $login ."' AND user_pass = '" . $password . "'";
        $array = dbQueryGetResult($query);
        if (!empty($array)) 
        {
            createSession('project_id', $array[0]['project_id']);
            $res = $array[0]['project_id'];
        }
        dbConnectClose();
        return $res;
    }

    function logout($sessionName, $destination)
    {
    	deleteCurrentSession($sessionName);
    	closeSession();
    	header('Location: ' . $destination . '');
    }

    function authVK($client_id, $client_secret, $redirect_uri, $url)
    {
        if (isset($_GET['code'])) 
        {
            $params = array(
                'client_id' => $client_id,
                'client_secret' => $client_secret,
                'code' => $_GET['code'],
                'redirect_uri' => $redirect_uri
                );

            $token = json_decode(file_get_contents('https://oauth.vk.com/access_token' . '?' . urldecode(http_build_query($params))), true);

            if (isset($token['access_token'])) 
            {
                $params = array(
                    'uids'         => $token['user_id'],
                    'fields'       => 'uid,first_name',
                    'access_token' => $token['access_token']
                    );

                $userInfo = json_decode(file_get_contents('https://api.vk.com/method/users.get' . '?' . urldecode(http_build_query($params))), true);
                if (isset($userInfo['response'][0]['uid'])) 
                {
                    $userInfo = $userInfo['response'][0];
                    return $userInfo;
                }
            }
        }
    }

    function authFb($client_id, $client_secret, $redirect_uri, $url)
    {
        if (isset($_GET['code'])) 
        {
            $params = array(
                'client_id'     => $client_id,
                'redirect_uri'  => $redirect_uri,
                'client_secret' => $client_secret,
                'code'          => $_GET['code']
                );

            $url = 'https://graph.facebook.com/oauth/access_token';

            $tokenInfo = null;
            parse_str(file_get_contents($url . '?' . http_build_query($params)), $tokenInfo);

            if (count($tokenInfo) > 0 && isset($tokenInfo['access_token'])) 
            {
                $params = array('access_token' => $tokenInfo['access_token'], 'fields' => 'id,email,first_name');
                $userInfo = json_decode(file_get_contents('https://graph.facebook.com/me' . '?' . urldecode(http_build_query($params))), true);
                if (isset($userInfo['id'])) 
                {
                    return $userInfo;
                }
            }
        }
    }

    function authGoogle($client_id, $client_secret, $redirect_uri, $url)
    {
        if (isset($_GET['code'])) 
        {

            $params = array(
                'client_id'     => $client_id,
                'client_secret' => $client_secret,
                'redirect_uri'  => $redirect_uri,
                'grant_type'    => 'authorization_code',
                'code'          => $_GET['code']
                );

            $url = 'https://accounts.google.com/o/oauth2/token';

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, urldecode(http_build_query($params)));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            $result = curl_exec($curl);
            curl_close($curl);
            $tokenInfo = json_decode($result, true);

            if (isset($tokenInfo['access_token'])) 
            {
                $params['access_token'] = $tokenInfo['access_token'];

                $userInfo = json_decode(file_get_contents('https://www.googleapis.com/oauth2/v1/userinfo' . '?' . urldecode(http_build_query($params))), true);
                if (isset($userInfo['id'])) 
                {
                    return $userInfo;
                }
            }
        }
    }

    function getSocialAuthData($authType)
    {
        switch ($authType) 
        {
            case 'vk':
                $userInfo = authVK(APP_ID_VK, APP_SECRET_CODE_VK, REDIRECT_URL_VK, OATH_URL_VK);
                $res = array(
                            'socialId'  => $userInfo['uid'], 
                            'projectId' => md5($userInfo['uid']), 
                            'firstName' => $userInfo['first_name'],
                            'userMail'  => '',
                            );
                break;

            case 'fb':
                $userInfo = authFb(APP_ID_FB, APP_SECRET_CODE_FB, REDIRECT_URL_FB, OATH_URL_FB);
                $res = array(
                            'socialId'  => $userInfo['id'], 
                            'projectId' => md5($userInfo['email']), 
                            'firstName' => $userInfo['first_name'],
                            'userMail'  => $userInfo['email'],
                            );
                break;

            case 'google':
                $userInfo = authGoogle(APP_ID_GOOGLE, APP_SECRET_CODE_GOOGLE, REDIRECT_URL_GOOGLE, OATH_URL_GOOGLE);
                $res = array(
                            'socialId'  => $userInfo['id'], 
                            'projectId' => md5($userInfo['email']), 
                            'firstName' => $userInfo['given_name'],
                            'userMail'  => $userInfo['email'],
                            );
                break;
            
            default:
                $res = ERROR_MESSAGE;
                break;
        }
        return $res;
    }

    function socialAuth($data)
    {
        dbInitialConnect(DATABASE);
        $query = "SELECT project_id FROM users WHERE social_user_id = '" . $data['socialId'] . "'";
        $array = dbQueryGetResult($query);
        if (!empty($array)) 
        {
            createSession('project_id', $array[0]['project_id']);
            header("Location: http://localhost:8888/onShopGen/pages/main.php?id=" . $array[0]['project_id'] . "");
        }
        else
        {
            $query = "INSERT INTO users (social_user_id, user_name, project_id, user_mail) VALUES ('" . $data['socialId'] . "', '" . $data['firstName'] . "', '" . $data['projectId'] . "', '" . $data['userMail'] . "')";
            if (dbQuery($query))
            {
                dbConnectClose();
                createSession('project_id', $data['projectId']);
                header("Location: http://localhost:8888/onShopGen/pages/main.php?id=" . $data['projectId'] . "");
            }
            else
            {
                dbConnectClose();
                echo ERROR_MESSAGE;
            }
        }
    }