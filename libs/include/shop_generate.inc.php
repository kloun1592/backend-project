<?php
    function shopGenerate($projectId, $userFolder, $twig)
    {
        $status = UNSUCCESS_REQUEST_STATUS;
        if (tryGenerateFilesAndFolders($userFolder, $twig)) 
        {
            if (tryCreateDbAndTables($projectId))
            {
                dbInitialConnect(DATABASE);
                $query = "UPDATE users SET shop_created = true WHERE project_id = '" . $projectId . "'";
                if (dbQuery($query))
                {
                    $status = SUCCESS_REQUEST_STATUS;
                }
                dbConnectClose();
            }
        }

        if ($status != UNSUCCESS_REQUEST_STATUS) 
        { 
            createZipArchive($userFolder, $projectId);
            if (sendProjectLink($projectId)) 
            {
                $status = SUCCESS_REQUEST_STATUS;
            } 
        }
        else
        {
            removeResidues($userFolder);
        }
        return $status;
    }