<?php
    function tryGenerateFilesAndFolders($userFolder, $twig)
    {
        $shopType = checkData(intval($_POST['shopType']), 'integer');
        $optionList = getOptionList(checkData($_POST['optionList'], 'array'));
        $status = false;
        
        if (tryCreateMainFilesAndFolders($shopType, $userFolder, ARRAY_OF_MAIN_FILES)) 
        {
            if (tryCreateAdditionalFilesAndFolders($shopType, $userFolder, ARRAY_OF_ADDITIONAL_FILES_AND_FOLDERS))
            {
                if (tryCreatePages($twig, $shopType, $userFolder, MAIN_PAGES))
                {
                    $status = true;
                }
            }
        }
        return $status;
    }