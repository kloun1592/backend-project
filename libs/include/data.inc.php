<?php
    function checkData($data, $dataType)
    {
        $res = false;
        if (isset($data)) 
        {
            if (!empty($data)) 
            {
                if (gettype($data) == $dataType) 
                {
                    $res = $data;
                }
            }
        }
        return $res;
    }