<?php
    function tryCreateDbAndTables($projectId)
    {
        dbInitialConnect('');
        $status = false;
        
        if (tryCreateDB($projectId) && tryUseDB($projectId)) 
        {
            if (tryCreateDeliveryTable() && tryCreatePaymentTable())
            {
                if (tryCreateManufacturerTable() && tryCreateCategoryTable() && tryCreateCustomerTable())
                {
                    if (tryCreateProductTable() && tryCreateOrderTable())
                    {
                        $status = true;
                    }
                }
            }
        }
        if ($status == false) 
        {
            dropResidues($projectId);
        }
        dbConnectClose();
        return $status;
    }