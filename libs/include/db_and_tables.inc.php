<?php
    function tryCreateDB($projectId)
    {
    	$query = "CREATE DATABASE project" . $projectId . " CHARACTER SET utf8 COLLATE utf8_general_ci";
        return dbQuery($query);
    }

    function tryUseDB($projectId)
    {
    	$query = "USE project" . $projectId . "";
        return dbQuery($query);
    }

    function tryCreateDeliveryTable()
    {
    	$query = "CREATE TABLE delivery(
    	                       delivery_id SERIAL PRIMARY KEY,
    	                       delivery_name VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
    	                       delivery_description VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT ''
    	                       )";
        return dbQuery($query);
    }

    function tryCreatePaymentTable()
    {
    	$query = "CREATE TABLE payment(
    	                       payment_id SERIAL PRIMARY KEY,
    	                       payment_name VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
    	                       payment_description VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT ''
    	                       )";
        return dbQuery($query);
    }

    function tryCreateManufacturerTable()
    {
    	$query = "CREATE TABLE manufacturer(
    	                       manufacturer_id SERIAL PRIMARY KEY,
    	                       manufacturer_name VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
    	                       manufacturer_description VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
    	                       manufacturer_logo VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT ''
    	                       )";
        return dbQuery($query);
    }

    function tryCreateCategoryTable()
    {
    	$query = "CREATE TABLE category(
    	                       category_id SERIAL PRIMARY KEY,
    	                       category_name VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
    	                       category_description VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
    	                       category_image VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
    	                       category_product_quantity INT NULL DEFAULT NULL
    	                       )";
        return dbQuery($query);
    }

    function tryCreateCustomerTable()
    {
    	$query = "CREATE TABLE customer(
    	                       customer_id SERIAL PRIMARY KEY,
    	                       customer_name VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
   	                           customer_pass VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',    	                       
    	                       customer_mail VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
   	                           customer_phone INT NULL DEFAULT NULL,
    	                       customer_address VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',   	                           
    	                       customer_avatar VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
    	                       customer_online_status BOOLEAN NOT NULL DEFAULT FALSE
    	                       )";
        return dbQuery($query);
    }

    function tryCreateProductTable()
    {
    	$query = "CREATE TABLE product(
    	                       product_id SERIAL PRIMARY KEY, 
    	                       product_category_id BIGINT UNSIGNED, FOREIGN KEY (product_category_id) REFERENCES category(category_id), 
    	                       product_manufacturer_id BIGINT UNSIGNED, FOREIGN KEY (product_manufacturer_id) REFERENCES manufacturer(manufacturer_id),
    	                       product_name VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
    	                       product_description VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
    	                       product_image VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
    	                       product_type VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
    	                       product_cost INT NULL DEFAULT NULL,
                               product_available_status BOOLEAN NOT NULL DEFAULT FALSE,
                               product_quantity INT NULL DEFAULT NULL
    	                       )";
        return dbQuery($query);
    }

    function tryCreateOrderTable()
    {
    	$query = "CREATE TABLE `order` ( 
                               `order_id` SERIAL NOT NULL,
    	                       order_product_id BIGINT UNSIGNED, FOREIGN KEY(order_product_id) REFERENCES product(product_id),  
    	                       order_category_id BIGINT UNSIGNED, FOREIGN KEY(order_category_id) REFERENCES category(category_id), 
    	                       order_customer_id BIGINT UNSIGNED, FOREIGN KEY(order_customer_id) REFERENCES customer(customer_id),
    	                       order_delivery_id BIGINT UNSIGNED, FOREIGN KEY(order_delivery_id) REFERENCES delivery(delivery_id),
    	                       order_payment_id BIGINT UNSIGNED, FOREIGN KEY(order_payment_id) REFERENCES payment(payment_id),
    	                       order_name VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
    	                       order_customer_name VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
    	                       order_total_cost INT NULL DEFAULT NULL,
    	                       order_product_quantity INT NULL DEFAULT NULL,
    	                       order_comment VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
    	                       order_status VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
    	                       product_cost INT NULL DEFAULT NULL,
                               product_available_status BOOLEAN NOT NULL DEFAULT FALSE,
                               product_quantity INT NULL DEFAULT NULL,
                               PRIMARY KEY (`order_id`)
    	                       )";
        return dbQuery($query);
    }

    function dropResidues($projectId)
    {
        $query = "DROP DATABASE project" . $projectId . "";
        dbQuery($query);
    }