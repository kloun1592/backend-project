<?php    
    function recursiveCopy($from, $to)
    {
        if(!file_exists($to))
        {
            mkdir($to);
        }
        if ($objs = glob($from . "/*")) 
        {
            foreach($objs as $obj) 
            {
                $forto = $to . str_replace($from, '', $obj);
                if(is_dir($obj))
                {
                    removeDirRec($obj, $forto);
                }
                else
                {
                    copy($obj, $forto);
                }
            }
        } 
    } 

    function getOptionList($optionList)
    {
        $arrayOfOptions = ARRAY_OF_OPTIONS;
        for ($i = 0; $i < count($optionList); $i++) 
        { 
            if (array_key_exists($optionList[$i], $arrayOfOptions)) 
            {
                $arrayOfOptions[$optionList[$i]] = true;
            }
        }
        return $arrayOfOptions;  
    }

    function tryCreateFile($source, $destiny)
    {
        $status = true;
        if(!copy($source, $destiny))
        {
            $status = false;
        }    
        return $status;
    }

    function tryCreateFolder($userFolder, $neededFolder)
    {
        $status = true;
        if(!mkdir($userFolder . $neededFolder))
        {
            $status = false;
        }
        return $status;
    }

    /*function tryCreateFilesByOptionList($arrayOfOptions, $shopType, $userFolder)
    {
        $status = true;
        $array = array_keys($arrayOfOptions, true);
        for ($i = 0; $i < count($array); $i++) 
        { 
            if (!copy('../templates/' . $shopType . '/css/' . $array[$i] . '.css', $userFolder . '/css/' . $array[$i] . '.css'))
            {
                $status = false;
            }
        }
        return $status;
    }*/

    function tryCreateMainFilesAndFolders($shopType, $userFolder, $mainFilesArray)
    {
        $status = true;
        $status = tryCreateFolder($userFolder, '');
        $status = tryCreateFolder($userFolder, '/css/');
        foreach ($mainFilesArray as $folderName) 
        {
            foreach ($folderName as $fileName) 
            {
                $currfolderName = array_keys($mainFilesArray, $folderName); 
                for ($i = 0; $i < count($currfolderName); $i++) 
                { 
                    $source = '../templates/' . $shopType . '/' .  $currfolderName[$i] . '/' . $fileName . '';
                    $destiny = $userFolder . '/' . $currfolderName[$i] . '/' . $fileName . '';
                    if (!tryCreateFile($source, $destiny)) 
                    {
                        $status = false;
                    } 
                }
            }
        }
        return $status;
    }

    function tryCreateAdditionalFilesAndFolders($shopType, $userFolder, $additionalFileAndFolfersArray)
    {
        $status = true;
        foreach ($additionalFileAndFolfersArray as $folderName) 
        {
            $source = '../templates/' . $shopType . '/' . $folderName . '/';
            $destiny = $userFolder . '/' . $folderName . '/';
            recursiveCopy($source, $destiny);
        }
        return $status;
    }

    function tryRenderPage($userFolder, $template, $pageName, $arrayOfOptions)
    {
        $status = true;
        if (fopen('' . $userFolder . '/' . $pageName . '.html', 'w')) 
        {
            $page = fopen('' . $userFolder . '/' . $pageName . '.html', 'w');
            if (!fwrite($page, $template->render($arrayOfOptions))) 
            {
                $status = false;
            }
            if (!fclose($page)) 
            {
                $status = false;
            }
        }
        else
        {
            $status = false;
        }
        return $status;
    }

    function tryCreatePages($twig, $shopType, $userFolder, $needablePages)
    {
        $status = true;
        for ($i = 0; $i < count($needablePages); $i++) 
        { 
            $template = $twig->loadTemplate('/' . $shopType . '/' . $needablePages[$i] . '.html');
            if (!tryRenderPage($userFolder, $template, $needablePages[$i], getOptionList(checkData($_POST['optionList'], 'array'))))
            {
                $status = false;
            }  
        }
        return $status;
    }

    function removeResidues($src) 
    {
        $dir = opendir($src);
        while(($file = readdir($dir)) !== false) 
        {
            if (($file != '.') && ($file != '..')) 
            {
                $full = $src . '/' . $file;
                if (is_dir($full)) 
                {
                    removeResidues($full);
                }
                else 
                {
                    unlink($full);
                }
            }
        }
        closedir($dir);
        rmdir($src);
    }

    function createZipArchive($userFolder, $projectId)
    {
        $rootPath = realpath($userFolder);
        $zip = new ZipArchive();
        $zip->open('project' . $projectId . '.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);
        $files = new RecursiveIteratorIterator(
                 new RecursiveDirectoryIterator($rootPath),
                 RecursiveIteratorIterator::LEAVES_ONLY
                 );

        foreach ($files as $name => $file)
        {
            if (!$file->isDir())
            {
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($rootPath) + 1);   
                $zip->addFile($filePath, $relativePath);
            }
        }

        $zip->close();
        mkdir('../projects/project' . $projectId . '/zip');
        $source = 'project' . $projectId . '.zip';
        $destiny = '../projects/project' . $projectId . '/zip/project' . $projectId . '.zip';
        rename($source, $destiny);
    }