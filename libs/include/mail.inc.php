<?php
    function sendMail($to, $subject, $txt, $headers)
    {
    	return mail($to, $subject, $txt, $headers);
    }

    function sendProjectLink($projectId)
    {
    	dbInitialConnect(DATABASE);
        $query = "SELECT user_mail FROM users WHERE project_id = '" . $projectId . "'";
        $array = dbQueryGetResult($query);
        dbConnectClose();
        if (!empty($array[0]['user_mail']))
        {
        	$to = $array[0]['user_mail'];
        }
        $link = 'http://localhost:8888/onShopGen/projects/project' . $projectId . '/zip/project' . $projectId . '.zip';
        $subject = 'Ссылка на загрузку интернет-магазина с сайта onShopGen';
        $txt = 'Привет! <br/> Сслыка на загрузку твоего интернет-магазина: <a href="' . $link . '">Ссылка</a>';
        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf8' . "\r\n";
        $headers .= "From: onShopGen.ru <onShopGen@bk.com> \r\n ";
        return sendMail($to, $subject, $txt, $headers);
    }