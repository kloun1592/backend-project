<?php
    function createSession($sessionName, $sessionValue)
    {
    	session_start();
        $_SESSION[$sessionName] = $sessionValue;
    }

    function getSession($sessionName)
    {
    	session_start();
    	return $_SESSION[$sessionName];
    }

    function createCookie($cookieName, $cookieValue, $cookieTime)
    {
    	setcookie($cookieName, $cookieValue, $cookieTime, "/");
    }

    function deleteCurrentSession($sessionName)
    {
        session_start();
        unset($_SESSION[$sessionName]);
    }

    function closeSession()
    {
        session_unset();
        session_destroy();
    }

    function getCookie($cookieName)
    {
    	return $_COOKIE[$cookieName];
    }