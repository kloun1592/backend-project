<?php
    require_once 'include/common.inc.php';
    
    $userLogin = checkData($_POST['user_login'], 'string');
    $userPass = checkData($_POST['user_pass'], 'string');

    echo login($userLogin, $userPass);