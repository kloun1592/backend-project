function openSwipe(indexNum)
{
	var pswpElement = document.querySelectorAll('.pswp')[0];
    var items = 
    [
        {
            src: '../web/img/shop_1.png',
            w: 1280,
            h: 3848
        },
        {
            src: '../web/img/shop_2.png',
            w: 1280,
            h: 4837
        },
        {
            src: '../web/img/shop_3.png',
            w: 1280,
            h: 4945
        }
    ];

    // define options (if needed)
    var options = 
    {
    // optionName: 'option value'
    // for example:
        index: indexNum,
        history: false,
        focus: false,
        shareEl: false,
        fullscreenEl: false,
        showHideOpacity: true,
        showAnimationDuration: 100,
        hideAnimationDuration: 100,
        barsSize: {top:0, bottom:0}, 
        bgOpacity: 0.8,
        loop: true
    };

// Initializes and opens PhotoSwipe
    var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
    gallery.init();
}
document.getElementsByClassName('shopImageFirst')[0].onclick = function()
{
	openSwipe(0);
}
document.getElementsByClassName('shopImageSecond')[0].onclick = function()
{
	openSwipe(1);
}
document.getElementsByClassName('shopImageThird')[0].onclick = function()
{
	openSwipe(2);
}