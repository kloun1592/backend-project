function successActions(data)
{
    hideLoading();
    $('.server-answer').show();
    if (data == 'repetedLogin') 
    {
        $('.server-answer').text('Логин, введёный вами уже используется. Пожалуйста, смнеите на другой.');
    }
    else
    {
        if (data == 'repetedMail') 
        {
            $('.server-answer').text('Email, введёный вами уже используется. Пожалуйста, смнеите на другой.'); 
        }
        else
        {
            if (data == 'badData') 
            {
                $('.server-answer').text('Введёные вами данные не соответствуют требованиям. Пожалуйста, проверьте их.');
            }
            else
            {
                window.location.replace('http://localhost:8888/onShopGen/pages/main.php?id=' + data);
            }
        }
    }
}

function errorActions(data)
{
    hideLoading();
    $('.server-answer').show();
    $('.server-answer').text('Чтото пошло не так.... Попробуйте позже.' + data);
}

$('form').submit(function(event) 
{
    if ($('#login').val() != '' && $('#pass').val() != '' && $('#mail').val() != '')
    {
            showLoading();
            var formData = 
            {
                'user_login': $('#login').val(),
                'user_pass': $('#pass').val(),
                'user_mail': $('#mail').val(),
            };
            $.ajax({
                type: 'POST',
                url : '../libs/registrer.php', 
                data: formData,
                success: function(data)
                {
                    successActions(data);
                },
                error: function(data)
                {
                    errorActions(data);
                } 
            })
    }
    else
    {
        $('.server-answer').show();
        $('.server-answer').text('Вы не заполнили необходимые поля. Заполните их пожалуйста.'); 
    }

    event.preventDefault();
});