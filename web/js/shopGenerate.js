function successActions(data)
{
    hideLoading();
    if (data == 'badData') 
    {
        showDialog(
        {
            title: 'Ой!',
            text: 'В процессе создания магазина что-то пошло не так :( <br/> Пожалуйста, попробуйте через 5 минут.',
            negative: 
            {
               title: 'Окей.....'
            },
            cancelable: false
        });
    }
    else
    {
        if(data == 'requestSuccess')
        {
            showDialog(
            {
                title: 'Подравляем!',
                text: 'Ваш интернет-магазин успешно создан! :) <br> Сейчас вам на почту придёт письмо с сслыкой на загрузку магазина.',
                positive: 
                {
                   title: 'Посмотреть магазин'
                },
                cancelable: false
            });
        }
        else
        {
            showDialog(
            {
                title: 'Ой!',
                text: 'Что-то пошло не так:( Пожалуйста, попробуйте позже.' + data,
                negative: 
                {
                   title: 'Окей.....'
                },
                cancelable: false
            });
        }
    }
}

function errorActions(data)
{
    hideLoading();
    showDialog(
    {
        title: 'Ой!',
        text: 'Чтото пошло не так:( Пожалуйста, попробуйте позже.' + data,
        negative: 
        {
           title: 'Окей.....'
        },
        cancelable: false
    });
}

$('form').submit(function(event) 
{
    showLoading();
    var optionList = new Array();
    $.each($('input:checkbox[name="optionList[]"]:checked'), function() 
    {
        optionList.push($(this).val());
    });

    if ($.isEmptyObject(optionList)) 
    {
        hideLoading();
        showDialog(
        {
            title: 'Ой!',
            text: 'Вы не выбрали ни одного пункта. Выберите пожалуйста.',
            negative: 
            {
                title: 'Хорошо, сейчас выберу'
            },
            cancelable: false
        }); 
    }
    else
    {
        var $_GET = getQueryParams(document.location.search);
        var formData = 
        {
            'shopType': $_GET['shopType'],
            'optionList': optionList,
        };

        $.ajax({
            type: 'POST',
            url : '../libs/shopGenerate.php', 
            data: formData,
            success: function(data)
            {
                successActions(data);
            },
            error: function(data)
            {
                errorActions(data);
            }
          })
    }

      event.preventDefault();
});