function successActions(data)
{
    hideLoading();
    $('.server-answer').show();
    if (data == 'badData') 
    {
        $('.server-answer').text('Вы ввели неккоректные данные. Проверьте логин и пароль.');
    }
    else
    {
        window.location.replace('http://localhost:8888/onShopGen/pages/main.php?id=' + data);
    }
}

function errorActions(data)
{
    hideLoading();
    $('.server-answer').show();
    $('.server-answer').text('Что-то пошло не так.... Попробуйте позже.' + data);
}

$('form').submit(function(event) 
{
    showLoading();
    var formData = 
    {
        'user_login': $('#login').val(),
        'user_pass': $('#pass').val(),
    };
    $.ajax({
        type: 'POST',
        url : 'libs/login.php', 
        data: formData,
        success: function(data)
        {
            successActions(data);
        },
        error: function(data)
        {
        	errorActions(data);
        }
      })

      event.preventDefault();
});