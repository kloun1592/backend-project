<?php
    require_once 'libs/include/consts/vk_consts.php';
    require_once 'libs/include/consts/google_consts.php';
    require_once 'libs/include/consts/fb_consts.php';
?>
<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Главная страница</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="web/css/material.indigo-pink.min.css">
    <link rel="stylesheet" type="text/css" href="web/css/main.css">
    <link rel="stylesheet" href="web/css/mdl-jquery-modal-dialog.css">
  </head>
  <body>
    <div class="mdl-card mdl-shadow--3dp">
      <h2 class="mdl-card__title-text">Вход</h2>
      <div id="preloader" class="preloader"><img src="web/img/preloader.gif" alt="preloader"></div>
      <span class="server-answer"></span>
      <form action="libs/login.php" method="POST" id="loginForm">
        <div class="mdl-textfield mdl-js-textfield">
          <input class="mdl-textfield__input" name="user_login" type="text" id="login">
          <label class="mdl-textfield__label" for="login">Ваш логин...</label>
        </div>
        <div class="mdl-textfield mdl-js-textfield">
          <input class="mdl-textfield__input" name="user_pass" type="password" id="pass">
          <label class="mdl-textfield__label" for="pass">Ваш пароль....</label>
        </div>
        <button class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored">
          Войти
        </button>  
        <h3 class="mdl-card__title-text h3">Войти с помошью соц. сетей</h3>
        <div class="social-buttons">
          <a href="<?= LINK_VK ?>"><img src="web/img/vkontakte-512.png" alt=""></a>
          <a href="<?= LINK_GOOGLE ?>"><img src="web/img/google_plus-square-social-media-128.png" alt=""></a>
          <a href="<?= LINK_FB ?>"><img src="web/img/square-facebook-512.png" alt=""></a>
        </div> 
      </form>
      <h3 class="mdl-card__title-text h3">Вы не зарегестрированы?</h3>
      <a class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" href="pages/registration.php">Зарегестрироваться</a>
    </div>
    <script defer src="web/js/material.min.js"></script>
    <script src="web/js/jquery.min.js"></script>
    <script src="web/js/mdl-jquery-modal-dialog.js"></script>
    <script src="web/js/login.js"></script>
  </body>
</html>